*** Settings ***
Library           Selenium2Library



*** Test Cases ***
Open Browser Advice
    Open Browser          https://developoneweb.advice.co.th/debug/true          gc
    Maximize Browser Window
    Alert Should Not Be Present          timeout=10


# Check_Menu_shopping_online
#     Mouse Down          //html/body/header[1]/div[2]/div/div/div[2]/div[2]/div[1]/div/ul/li[2]/a
#      Element Text Should Be          
#     Click Element          //html/body/header[1]/div[2]/div/div/div[2]/div[2]/div[1]/div/ul/li[2]/div/div/div/ul/li[1]/a

# Check_Menu_NOTEBOOK
#     Mouse Down          xpath://*[@id="primary_nav_wrap"]/ul/li[1]/a
# #เช็คเมนูหลัก Notebook
#     Element Text Should Be          //*[@id="primary_nav_wrap"]/ul/li[1]/a          NOTEBOOK
# #เช็คเมนูย่อย1 Notebook
#     Element Text Should Be          //*[@id="primary_nav_wrap"]/ul/li[1]/ul/div/div[1]/div/div[1]/div[1]/a          Notebook
# #คลิก + Notebook
#     Click Element          xpath://*[@id="primary_nav_wrap"]/ul/li[1]/ul/div/div[1]/div/div[1]/div[1]/i
#     Alert Should Not Be Present          timeout=2
# #เช็คเมนูย่อย 2 Notebook
#     Element Text Should Be          //*[@id="menu-left-notebook"]/a[1]          Notebook ACER
#     Element Text Should Be          //*[@id="menu-left-notebook"]/a[2]          Notebook ASUS
#     Element Text Should Be          //*[@id="menu-left-notebook"]/a[3]          Notebook LENOVO
#     Element Text Should Be          //*[@id="menu-left-notebook"]/a[4]          Notebook HUAWEI
#     Element Text Should Be          //*[@id="menu-left-notebook"]/a[5]          Notebook MSI
#     Element Text Should Be          //*[@id="menu-left-notebook"]/a[6]          Notebook DELL
#     Element Text Should Be          //*[@id="menu-left-notebook"]/a[7]          Notebook HP
#     #Element Text Should Be          //*[@id="menu-left-notebook"]/a[8]          Notebook ACER+
# #เช็คเมนูย่อย1 Notebook Gaming
#     Element Text Should Be          //*[@id="primary_nav_wrap"]/ul/li[1]/ul/div/div[2]/div/div[1]/div[1]/a          Notebook Gaming
# #คลิก + Notebook Gaming
#     Click Element          xpath://*[@id="primary_nav_wrap"]/ul/li[1]/ul/div/div[2]/div/div[1]/div[1]/i
#     Alert Should Not Be Present          timeout=2
# #เช็คเมนูย่อย 2 Notebook Gaming
#     Element Text Should Be          //*[@id="menu-left-notebook-gaming"]/a[1]          Notebook Gaming ASUS
#     Element Text Should Be          //*[@id="menu-left-notebook-gaming"]/a[2]          Notebook Gaming ACER
#     Element Text Should Be          //*[@id="menu-left-notebook-gaming"]/a[3]          Notebook Gaming LENOVO
#     Element Text Should Be          //*[@id="menu-left-notebook-gaming"]/a[4]          Notebook Gaming DELL
#     Element Text Should Be          //*[@id="menu-left-notebook-gaming"]/a[5]          Notebook Gaming HP
# #เช็คเมนูย่อย1 Notebook 2in1
#     Element Text Should Be          //*[@id="primary_nav_wrap"]/ul/li[1]/ul/div/div[3]/div/div[1]/div[1]/a          Notebook 2in1
# #คลิก + Notebook 2in1
#     Click Element          xpath://*[@id="primary_nav_wrap"]/ul/li[1]/ul/div/div[3]/div/div[1]/div[1]/i
#     Alert Should Not Be Present          timeout=2
# #เช็คเมนูย่อย 2 Notebook 2in1
#     Element Text Should Be          //*[@id="menu-left-notebook-2in1"]/a[1]          Notebook 2in1 ASUS
#     Element Text Should Be          //*[@id="menu-left-notebook-2in1"]/a[2]          Notebook 2in1 LENOVO
#     Element Text Should Be          //*[@id="menu-left-notebook-2in1"]/a[3]          Notebook 2in1 DELL
#     Element Text Should Be          //*[@id="menu-left-notebook-2in1"]/a[4]          Notebook 2in1 HP


# Check_Menu_MEMORY CARD & READER
#     Mouse Down          xpath://*[@id="primary_nav_wrap"]/ul/li[7]/a
# #เช็คเมนูหลัก MEMORY CARD & READER
#     Element Text Should Be          //*[@id="primary_nav_wrap"]/ul/li[7]/a          MEMORY CARD & READER
# #เช็คเมนูย่อย1 Micro SD Card
#     Element Text Should Be          //*[@id="primary_nav_wrap"]/ul/li[7]/ul/div/div[1]/div/div[1]/div[1]/a          Micro SD Card
# #คลิก + Micro SD Card
#     Click Element          xpath://*[@id="primary_nav_wrap"]/ul/li[7]/ul/div/div[1]/div/div[1]/div[1]/i
#     Alert Should Not Be Present          timeout=2
# #เช็คเมนูย่อย 2 Micro SD Card
#     Element Text Should Be          //*[@id="menu-left-micro-sd-card"]/a[1]          MICRO SD CARD 4-8 GB
#     Element Text Should Be          //*[@id="menu-left-micro-sd-card"]/a[2]          MICRO SD CARD 16 GB
#     Element Text Should Be          //*[@id="menu-left-micro-sd-card"]/a[3]          MICRO SD CARD 32 GB
#     Element Text Should Be          //*[@id="menu-left-micro-sd-card"]/a[4]          MICRO SD CARD 64 GB
#     Element Text Should Be          //*[@id="menu-left-micro-sd-card"]/a[5]          MICRO SD CARD 128 GB
#     Element Text Should Be          //*[@id="menu-left-micro-sd-card"]/a[6]          MICRO SD CARD 200 - 256 GB
#     Alert Should Not Be Present          timeout=2
#   #execute javascript   Window.scrollTo   (0,document.body.scrollHeight)
#   #Set Window Size 	800	600
#   #Set Window Position	100	200

# Check_Menu_SMARTPHONE & TABLET
#     Mouse Down        xpath://*[@id="primary_nav_wrap"]/ul/li[22]/a
#     Alert Should Not Be Present          timeout=2
# #เช็คเมนูหลัก SMARTPHONE & TABLET
#     Element Text Should Be          //*[@id="primary_nav_wrap"]/ul/li[22]/a          SMARTPHONE & TABLET
# #เช็คเมนูย่อย1 Smartphone
#     Element Text Should Be          //*[@id="primary_nav_wrap"]/ul/li[22]/ul/div/div[1]/div/div[1]/div[1]/a          Smartphone
# #คลิก + Smartphone
#     Click Element          //*[@id="primary_nav_wrap"]/ul/li[22]/ul/div/div[1]/div/div[1]/div[1]/i
#     Alert Should Not Be Present          timeout=2
# #เช็คเมนูย่อย 2 Smartphone
#     Element Text Should Be          //*[@id="menu-left-smartphone"]/a[1]          Smartphone ASUS
#     Element Text Should Be          //*[@id="menu-left-smartphone"]/a[2]          Smartphone HUAWEI
#     Element Text Should Be          //*[@id="menu-left-smartphone"]/a[3]          Smartphone SAMSUNG
#     Element Text Should Be          //*[@id="menu-left-smartphone"]/a[4]          Smartphone VIVO
#     Element Text Should Be          //*[@id="menu-left-smartphone"]/a[5]          Smartphone XIAOMI
#     Element Text Should Be          //*[@id="menu-left-smartphone"]/a[6]          Smartphone APPLE

#     Alert Should Not Be Present          timeout=2
# Check_menu_under
#     click Element          //html/body/footer[2]/div/div/div/div[2]/div/div[1]/ul/li[10]/a
#เช็คเมนูย่อย1 Tablet
#คลิก + Tablet
#เช็คเมนูย่อย 2 Tablet


#เทสกรณีรอ 3 นาทีอยู่ในเมนู 1
Check_click_Menu1_UPS
     Click Element          //*[@id="primary_nav_wrap"]/ul/li[12]/a
     Alert Should Not Be Present          timeout=5
Check_image_qr_code_UPS
     Page Should Contain Image         https://img.advice.co.th/images_nas/advice/oneweb/assets/images/qr_code_line_advice.png
     Alert Should Not Be Present          timeout=5
#เทสกรณีรอ 3 นาทีอยู่ในเมนู 2
Check_click_Mouse Down_Menu1_NOTEBOOK ACCESSORIES
     Mouse Down          //*[@id="primary_nav_wrap"]/ul/li[2]/a
     Alert Should Not Be Present          timeout=5
Check_click_Menu2_NOTEBOOK ACCESSORIES
     Click Element          //*[@id="primary_nav_wrap"]/ul/li[2]/ul/div/div[4]/div/div[1]/div[1]/a
     Alert Should Not Be Present          timeout=5
Check_image_qr_code_Adapter & Battery Dell
     Page Should Contain Image         https://img.advice.co.th/images_nas/advice/oneweb/assets/images/qr_code_line_advice.png